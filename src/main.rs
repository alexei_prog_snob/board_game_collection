mod bgg;

fn main() {
    let bgg_username = "alexeir";
    match bgg::get_collection(&bgg_username) {
        Ok(games) => {
            println!("{}'s collection:", bgg_username);
            for game in games {
                println!("{}", game);
            }
        },
        Err(e) => println!("error {e:?}"),
    }
}
