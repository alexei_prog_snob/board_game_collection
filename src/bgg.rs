use reqwest::blocking::{Client, Response};
use std::io::Read;
use xml::reader::{EventReader, XmlEvent};

fn get_from_xml(response: &mut Response) -> Result<Vec<String>, Box<dyn std::error::Error>> {
    let mut xml = String::new();
    response.read_to_string(&mut xml)?;

    let parser = EventReader::new(xml.as_bytes());
    let mut in_item = false;
    let mut current_game = String::new();
    let mut games = Vec::new();


    for e in parser {
        match e? {
            XmlEvent::StartElement { name, .. } => {
                if name.local_name == "name" {
                    in_item = true;
                }
            }
            XmlEvent::EndElement { name } => {
                if name.local_name == "name" {
                    in_item = false;
                    games.push(current_game.clone());
                    current_game.clear();
                }
            }
            XmlEvent::Characters(chars) => {
                if in_item {
                    current_game.push_str(&chars);
                }
            }
            _ => {}
        }
    }

    Ok(games)
}


pub fn get_collection(bgg_username: &str) -> Result<Vec<String>, Box<dyn std::error::Error>> {
    let bgg_url = format!(
        "https://www.boardgamegeek.com/xmlapi2/collection?username={}&own=1",
        bgg_username);
    
    let client = Client::new();
    get_from_xml(&mut client.get(&bgg_url).send()?)
}